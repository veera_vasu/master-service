package com.vv.master.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vv.commonutilis.dto.CommonDataTableDto;
import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.master.dto.EmployeeDetailsDto;
import com.vv.master.dto.FormPagesDto;
import com.vv.master.dto.MenuPagesDto;
import com.vv.master.dto.ProjectDetailsDto;
import com.vv.master.dto.RoleDetailsDto;
import com.vv.master.dto.TeamDetailsDto;
import com.vv.master.dto.UserDetailsDto;
import com.vv.master.service.MasterService;

/**
 * @author Aswani.N
 * 
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/vv/master/api/")
public class MasterController {

	@Autowired
	MasterService masterService;

	Logger logger = LoggerFactory.getLogger(MasterController.class);

	@PostMapping(value = "saveUserDetails")
	public ResponseEntity<?> saveUserDetails(@RequestBody UserDetailsDto dto) {
		logger.info("MasterController-saveUserDetails-start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<>();
		response = masterService.saveUserDetails(dto);
		logger.info("MasterController-saveUserDetails-End");
		return new ResponseEntity<CommonReponseDto<UserDetailsDto>>(response, HttpStatus.OK);
	}

	@PostMapping(value = "saveRoleDetails")
	public ResponseEntity<?> saveRoleDetails(@RequestBody RoleDetailsDto dto) {
		logger.info("MasterController-saveRoleDetails-start");
		CommonReponseDto<RoleDetailsDto> response = new CommonReponseDto<RoleDetailsDto>();
		response = masterService.saveRoleDetails(dto);
		logger.info("MasterController-saveRoleDetails-End");
		return new ResponseEntity<CommonReponseDto<RoleDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchUserProfile")
	public ResponseEntity<?> fetchUserProfile(@RequestParam String payLoad) {
		logger.info("MasterController-fetchUserProfile-start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<UserDetailsDto>();
		response = masterService.fetchUserProfile(payLoad);
		logger.info("MasterController-fetchUserProfile-End");
		return new ResponseEntity<CommonReponseDto<UserDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchMenuPages")
	public ResponseEntity<?> fetchMenuPages(@RequestParam String payLoad) {
		logger.info("MasterController-fetchMenuPages-start");
		CommonReponseDto<MenuPagesDto> response = new CommonReponseDto<MenuPagesDto>();
		response = masterService.fetchMenuPages(payLoad);
		logger.info("MasterController-fetchMenuPages-End");
		return new ResponseEntity<CommonReponseDto<MenuPagesDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchFormPage")
	public ResponseEntity<?> fetchFormPage(@RequestParam String payLoad) {
		logger.info("MasterController-fetchFormPage-start");
		CommonReponseDto<FormPagesDto> response = new CommonReponseDto<FormPagesDto>();
		response = masterService.fetchFormPage(payLoad);
		logger.info("MasterController-fetchFormPage-End");
		return new ResponseEntity<CommonReponseDto<FormPagesDto>>(response, HttpStatus.OK);
	}

	@PostMapping(value = "fetchAllUsers")
	public ResponseEntity<?> fetchAllUsers(@RequestBody CommonDataTableDto dto) {
		logger.info("MasterController-fetchAllUsers-start");
		CommonDataTableDto response = new CommonDataTableDto();
		response = masterService.fetchAllUsers(dto);
		logger.info("MasterController-fetchAllUsers-End");
		return new ResponseEntity<CommonDataTableDto>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchEmployeeDetails")
	public ResponseEntity<?> fetchEmployeeDetails() {
		logger.info("MasterController-fetchEmployeeDetails-start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		response = masterService.fetchEmployeeDetails();
		logger.info("MasterController-fetchEmployeeDetails-End");
		return new ResponseEntity<CommonReponseDto<EmployeeDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchAllEmployeedetails")
	public ResponseEntity<?> fetchAllEmployeedetails() {
		logger.info("MasterController-fetchAllEmployeedetails-start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		response = masterService.fetchAllEmployeedetails();
		logger.info("MasterController-fetchAllEmployeedetails-End");
		return new ResponseEntity<CommonReponseDto<EmployeeDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchallProjects")
	public ResponseEntity<?> fetchallProjects() {
		logger.info("MasterController-fetchAllEmployeedetails-start");
		CommonReponseDto<ProjectDetailsDto> response = new CommonReponseDto<ProjectDetailsDto>();
		response = masterService.fetchallProjects();
		logger.info("MasterController-fetchallProjects-End");
		return new ResponseEntity<CommonReponseDto<ProjectDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchAllTeamDetails")
	public ResponseEntity<?> fetchAllTeamDetails() {
		logger.info("MasterController-fetchAllTeamDetails-start");
		CommonReponseDto<TeamDetailsDto> response = new CommonReponseDto<TeamDetailsDto>();
		response = masterService.fetchAllTeamDetails();
		logger.info("MasterController-fetchAllTeamDetails-End");
		return new ResponseEntity<CommonReponseDto<TeamDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchProjectId")
	public ResponseEntity<?> fetchProjectId(@RequestParam String payLoad) {
		logger.info("MasterController-fetchProjectId-start");
		CommonReponseDto<ProjectDetailsDto> response = new CommonReponseDto<ProjectDetailsDto>();
		response = masterService.fetchProjectId(payLoad);
		logger.info("MasterController-fetchProjectId-End");
		return new ResponseEntity<CommonReponseDto<ProjectDetailsDto>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "fetchEmployeeId")
	public ResponseEntity<?> fetchEmployeeID(@RequestParam String payLoad) {
		logger.info("MasterController-fetchEmployeeId-start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		response = masterService.fetchEmployeeId(payLoad);
		logger.info("MasterController-fetchEmployeeId-End");
		return new ResponseEntity<CommonReponseDto<EmployeeDetailsDto>>(response, HttpStatus.OK);
	}
}
