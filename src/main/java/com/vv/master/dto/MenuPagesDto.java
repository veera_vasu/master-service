/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Jyothi
 *
 */
public class MenuPagesDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String code;
	private String name;
	private String icon;
	private int orderNo;
	private Boolean isActive;
	private String attributes;
	private String visibleRoles;
	private String url;
	private Date timeStamp;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getIcon() {
		return icon;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getAttributes() {
		return attributes;
	}

	public String getVisibleRoles() {
		return visibleRoles;
	}

	public String getUrl() {
		return url;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public void setVisibleRoles(String visibleRoles) {
		this.visibleRoles = visibleRoles;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
