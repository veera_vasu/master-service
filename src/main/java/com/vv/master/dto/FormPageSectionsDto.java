/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jyothi.ch
 *
 */
public class FormPageSectionsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String additionalInfo;
	private String description;
	private String displayName;
	private Boolean isActive;
	private Boolean isQuestionaire;
	private String objName;
	private int orderNo;
	private String sectionCode;
	private Long pageId;
	private Date createdDate;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public String getDescription() {
		return description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsQuestionaire() {
		return isQuestionaire;
	}

	public String getObjName() {
		return objName;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public String getSectionCode() {
		return sectionCode;
	}

	public Long getPageId() {
		return pageId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsQuestionaire(Boolean isQuestionaire) {
		this.isQuestionaire = isQuestionaire;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
