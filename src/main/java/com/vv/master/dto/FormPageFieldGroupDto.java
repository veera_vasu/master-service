/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jyothi ch
 *
 */
public class FormPageFieldGroupDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String additionalInfo;
	private String description;
	private String displayName;
	private Long fieldGroupCode;
	private Boolean isActive;
	private String model;
	private int orderNo;
	private Long subSectionId;
	private Date createdDate;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public String getDescription() {
		return description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Long getFieldGroupCode() {
		return fieldGroupCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getModel() {
		return model;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public Long getSubSectionId() {
		return subSectionId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setFieldGroupCode(Long fieldGroupCode) {
		this.fieldGroupCode = fieldGroupCode;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setSubSectionId(Long subSectionId) {
		this.subSectionId = subSectionId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
