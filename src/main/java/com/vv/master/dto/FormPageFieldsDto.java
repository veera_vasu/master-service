/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jyothi ch
 *
 */
public class FormPageFieldsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String attributes;
	private String description;
	private String displayName;
	private String fields;
	private String fieldcode;
	private Boolean isActive;
	private Boolean isMandatory;
	private int orderNo;
	private Long fieldGroupId;
	private Date createdDate;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getAttributes() {
		return attributes;
	}

	public String getDescription() {
		return description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getFields() {
		return fields;
	}

	public String getFieldcode() {
		return fieldcode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public Long getFieldGroupId() {
		return fieldGroupId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public void setFieldcode(String fieldcode) {
		this.fieldcode = fieldcode;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setFieldGroupId(Long fieldGroupId) {
		this.fieldGroupId = fieldGroupId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
