/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jyothi ch
 *
 */
public class FormPageSubSectionsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String additionalInfo;
	private String description;
	private String displayName;
	private Boolean isActive;
	private int orderNo;
	private String subSectionCode;
	private Long sectionId;
	private Date createdDate;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public String getDescription() {
		return description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public String getSubSectionCode() {
		return subSectionCode;
	}

	public Long getSectionId() {
		return sectionId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setSubSectionCode(String subSectionCode) {
		this.subSectionCode = subSectionCode;
	}

	public void setSectionId(Long sectionId) {
		this.sectionId = sectionId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
