/**
 * 
 */
package com.vv.master.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jyothi.ch
 *
 */
public class FormPagesDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String additionalInfo;
	private String description;
	private String displayName;
	private Boolean isActive;
	private String objName;
	private Long moduleId;
	private String pageCode;
	private int orderNo;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public String getDescription() {
		return description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getObjName() {
		return objName;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public String getPageCode() {
		return pageCode;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
