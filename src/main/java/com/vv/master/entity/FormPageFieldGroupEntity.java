/**
 * 
 */
package com.vv.master.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author jyothi ch
 *
 */
@Entity
@Table(name = "form_page_field_group")
public class FormPageFieldGroupEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "additional_info", length = 5000)
	private String additionalInfo;

	@Column(name = "description", length = 5000)
	private String description;

	@Column(name = "display_name", length = 300)
	private String displayName;

	@Column(name = "field_group_code", length = 20)
	private Long fieldGroupCode;

	@Column(name = "is_active", nullable = false)
	private Boolean isActive;

	@Column(name = "model", length = 5000)
	private String model;

	@Column(name = "order_no", length = 10)
	private int orderNo;

	@Column(name = "subsection_id", length = 20)
	private Long subSectionId;

	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "modified_date")
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Long getFieldGroupCode() {
		return fieldGroupCode;
	}

	public void setFieldGroupCode(Long fieldGroupCode) {
		this.fieldGroupCode = fieldGroupCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public Long getSubSectionId() {
		return subSectionId;
	}

	public void setSubSectionId(Long subSectionId) {
		this.subSectionId = subSectionId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
