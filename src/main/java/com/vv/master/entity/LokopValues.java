/**
 * 
 */
package com.vv.master.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author jyothi ch
 *
 */
@Entity
@Table(name = "lokop_values")
public class LokopValues implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "lokop_type_id", length = 100)
	private String lokopTypeId;

	@Column(name = "lokop_val_id", length = 100)
	private String lokopValId;

	@Column(name = "lokop_value", length = 400)
	private String lokopValue;

	@Column(name = "is_active", nullable = false)
	private Boolean isActive;

	@Column(name = "created_by", length = 100, nullable = false)
	private String createdBy;

	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "modified_by", length = 100)
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLokopTypeId() {
		return lokopTypeId;
	}

	public void setLokopTypeId(String lokopTypeId) {
		this.lokopTypeId = lokopTypeId;
	}

	public String getLokopValId() {
		return lokopValId;
	}

	public void setLokopValId(String lokopValId) {
		this.lokopValId = lokopValId;
	}

	public String getLokopValue() {
		return lokopValue;
	}

	public void setLokopValue(String lokopValue) {
		this.lokopValue = lokopValue;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
