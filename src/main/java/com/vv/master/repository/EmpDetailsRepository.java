package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.EmployeeDetails;

@Repository
public interface EmpDetailsRepository extends JpaRepository<EmployeeDetails, Long> {

	EmployeeDetails findByempidAndIsActive(String empId, Boolean true1);

}
