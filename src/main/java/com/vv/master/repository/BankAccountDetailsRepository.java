/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vv.master.entity.BankAccountDetailsEntity;

/**
 * 
 */
public interface BankAccountDetailsRepository extends JpaRepository<BankAccountDetailsEntity, Long> {

}
