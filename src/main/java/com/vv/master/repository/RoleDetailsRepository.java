package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.RoleDetailsEntity;
@Repository
public interface RoleDetailsRepository extends JpaRepository<RoleDetailsEntity, Long> {

}
