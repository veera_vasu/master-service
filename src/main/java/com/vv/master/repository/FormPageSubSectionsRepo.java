/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.FormPageSubSectionsEntity;

/**
 * @author jyothi ch
 *
 */
@Repository
public interface FormPageSubSectionsRepo extends JpaRepository<FormPageSubSectionsEntity, Long>{

}
