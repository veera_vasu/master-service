/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vv.master.entity.ProjectDetailsEntity;

/**
 * 
 */
public interface ProjectDetailsRepository extends JpaRepository<ProjectDetailsEntity, Long>{

	ProjectDetailsEntity findByprojectIdAndIsActive(String projectId, Boolean true1);

}
