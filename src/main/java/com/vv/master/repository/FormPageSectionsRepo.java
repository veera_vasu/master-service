/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.FormPageSectionsEntity;

/**
 * @author jyothi ch
 *
 */
@Repository
public interface FormPageSectionsRepo extends JpaRepository<FormPageSectionsEntity, Long>{

}
