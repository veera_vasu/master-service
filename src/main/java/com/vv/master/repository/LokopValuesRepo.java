/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.LokopValues;

/**
 * @author jyothi.ch
 *
 */
@Repository
public interface LokopValuesRepo extends JpaRepository<LokopValues, Long> {

}
