/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.FormPages;

/**
 * @author Jyothi ch
 *
 */
@Repository
public interface FormPagesRepo extends JpaRepository<FormPages, Long> {

	FormPages findByModuleTypeAndIsActive(String moduletype, Boolean true1);

}
