package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.TeamDetails;

@Repository
public interface TeamDetailsRepository extends JpaRepository<TeamDetails, Long> {

}