
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.MenuPages;

/**
 * @author jyothi ch
 *
 */
@Repository
public interface MenuPagesRepo extends JpaRepository<MenuPages, Long> {

	MenuPages findByVisibleRolesAndIsActive(String visibleRoles, Boolean true1);

}
