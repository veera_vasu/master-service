package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.TechnologyDetails;

@Repository
public interface TechnologyDetailsRepository extends JpaRepository<TechnologyDetails, Long> {

}
