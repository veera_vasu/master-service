/**
 * 
 */
package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vv.master.entity.FormPageFieldGroupEntity;

/**
 * @author jyothi ch
 *
 */
@Repository
public interface FormPageFieldGroupRepo extends JpaRepository<FormPageFieldGroupEntity, Long>{

}
