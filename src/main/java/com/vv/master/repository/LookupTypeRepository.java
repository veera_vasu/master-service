package com.vv.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vv.master.entity.LookupTypeEntity;

public interface LookupTypeRepository extends JpaRepository<LookupTypeEntity, Long> {

}
