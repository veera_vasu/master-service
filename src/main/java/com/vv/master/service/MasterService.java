package com.vv.master.service;

import com.vv.commonutilis.dto.CommonDataTableDto;
import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.master.dto.EmployeeDetailsDto;
import com.vv.master.dto.FormPagesDto;
import com.vv.master.dto.MenuPagesDto;
import com.vv.master.dto.ProjectDetailsDto;
import com.vv.master.dto.RoleDetailsDto;
import com.vv.master.dto.TeamDetailsDto;
import com.vv.master.dto.UserDetailsDto;

/**
 * @author Aswani.N
 *
 */
public interface MasterService {

	CommonReponseDto<UserDetailsDto> saveUserDetails(UserDetailsDto dto);

	CommonReponseDto<RoleDetailsDto> saveRoleDetails(RoleDetailsDto dto);

	CommonReponseDto<UserDetailsDto> fetchUserProfile(String payLoad);

	CommonReponseDto<FormPagesDto> fetchFormPage(String payLoad);

	CommonReponseDto<MenuPagesDto> fetchMenuPages(String payLoad);

//	CommonDataTableDto fetchAllUsers(String pageNo, String pageSize, String sortBy, String payLoad, int i);

	CommonReponseDto<EmployeeDetailsDto> fetchEmployeeDetails();

	CommonReponseDto<EmployeeDetailsDto> fetchAllEmployeedetails();

	CommonReponseDto<ProjectDetailsDto> fetchallProjects();

	CommonReponseDto<TeamDetailsDto> fetchAllTeamDetails();

	CommonReponseDto<ProjectDetailsDto> fetchProjectId(String payLoad);

	CommonReponseDto<EmployeeDetailsDto> fetchEmployeeId(String payLoad);

	CommonDataTableDto fetchAllUsers(CommonDataTableDto dto);

}
