/**
 * 
 */
package com.vv.master.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.vv.commonutilis.DateUtils;
import com.vv.commonutilis.JsonRequest;
import com.vv.commonutilis.dto.CommonDataTableDto;
import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.commonutilis.dto.CommonRequestDto;
import com.vv.commonutilis.dto.CommonSearchFieldDto;
import com.vv.commonutilis.services.CommonConstants;
import com.vv.master.dto.EmployeeDetailsDto;
import com.vv.master.dto.FormPagesDto;
import com.vv.master.dto.MenuPagesDto;
import com.vv.master.dto.ProjectDetailsDto;
import com.vv.master.dto.RoleDetailsDto;
import com.vv.master.dto.TeamDetailsDto;
import com.vv.master.dto.UserDetailsDto;
import com.vv.master.entity.EmployeeDetails;
import com.vv.master.entity.FormPages;
import com.vv.master.entity.MenuPages;
import com.vv.master.entity.ProjectDetailsEntity;
import com.vv.master.entity.RoleDetailsEntity;
import com.vv.master.entity.TeamDetails;
import com.vv.master.entity.UserDetailsEntity;
import com.vv.master.repository.EmpDetailsRepository;
import com.vv.master.repository.FormPagesRepo;
import com.vv.master.repository.MenuPagesRepo;
import com.vv.master.repository.ProjectDetailsRepository;
import com.vv.master.repository.RoleDetailsRepository;
import com.vv.master.repository.TeamDetailsRepository;
import com.vv.master.repository.UserDetailsRepository;
import com.vv.master.service.MasterService;

/**
 * @author Aswani.N
 *
 */
@Service
public class MasterServiceImpl implements MasterService {

	@Autowired
	RoleDetailsRepository roleDetailsRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	MenuPagesRepo menuPagesRepo;

	@Autowired
	FormPagesRepo formPagesRepo;

	@Autowired
	EmpDetailsRepository empDetailsRepository;

	@Autowired
	ProjectDetailsRepository projectDetailsRepository;

	@Autowired
	TeamDetailsRepository teamDetailsRepository;

	@PersistenceContext
	private EntityManager entityManager;

	Logger logger = LoggerFactory.getLogger(MasterServiceImpl.class);

	@Override
	public CommonReponseDto<UserDetailsDto> saveUserDetails(UserDetailsDto dto) {
		logger.info("MasterServiceImpl-saveUserDetails-Start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<>();
		try {
			if (null != dto) {
				UserDetailsEntity entity = new UserDetailsEntity();
				BeanUtils.copyProperties(dto, entity);
				entity.setCreatedBy("Admin");
				entity.setCreatedDate(DateUtils.sysDate());
				entity.setModifiedDate(new Date());
				entity = userDetailsRepository.save(entity);
				dto.setCreatedDate(DateUtils.convertDateTimeToStringWithPeriod(entity.getCreatedDate()));
				dto.setId(entity.getId());
				response.setResponsData(dto);
				response.setStatus(CommonConstants.SUCCESS);
				response.setMessage(CommonConstants.SAVE_MESSAGE);
			} else {
				response.setStatus(CommonConstants.ERROR);
				response.setMessage(CommonConstants.NO_USER_FOUND);
			}
		} catch (Exception e) {
			logger.error("Exception in MasterServiceImpl-saveUserDetails", e);
		}
		logger.info("MasterServiceImpl-saveUserDetails-End");
		return response;
	}

	@Override
	public CommonReponseDto<RoleDetailsDto> saveRoleDetails(RoleDetailsDto dto) {
		logger.info("MasterServiceImpl-saveRoleDetails-Start");
		CommonReponseDto<RoleDetailsDto> resopnse = new CommonReponseDto<RoleDetailsDto>();
		try {
			RoleDetailsEntity rolesdetailsentity = new RoleDetailsEntity();
			rolesdetailsentity.setCode(dto.getCode());
			rolesdetailsentity.setCreatedBy(dto.getCreatedBy());
			rolesdetailsentity.setCreatedDate(new Date());
			rolesdetailsentity.setModifiedDate(new Date());
			rolesdetailsentity = roleDetailsRepository.save(rolesdetailsentity);
			dto.setCreatedDate(rolesdetailsentity.getCreatedDate());
			dto.setId(rolesdetailsentity.getId());
			resopnse.setResponsData(dto);
			resopnse.setMessage(CommonConstants.SAVE_MESSAGE);
		} catch (Exception e) {
			logger.error("Exception in MasterServiceImpl-saveRoleDetails", e);
		}
		logger.info("MasterServiceImpl-saveRoleDetails-Start");
		return resopnse;
	}

	@Override
	public CommonReponseDto<UserDetailsDto> fetchUserProfile(String payLoad) {
		logger.info("MasterServiceImpl-fetchUserProfile-Start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<UserDetailsDto>();
		JsonRequest<CommonRequestDto> jsonRequest = new JsonRequest<>();
		try {
			CommonRequestDto dto = jsonRequest.readEncoded(payLoad, CommonRequestDto.class);
			UserDetailsEntity entity = userDetailsRepository.findByUseridAndIsActive(dto.getUserId(),
					CommonConstants.TRUE);
			if (null != entity && null != entity.getEmailId()) {
				UserDetailsDto resdto = new UserDetailsDto();
				BeanUtils.copyProperties(entity, resdto);
				response.setResponsData(resdto);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchUserProfile", e);
		}
		logger.info("MasterServiceImpl-fetchUserProfile-End");
		return response;
	}

	@Override
	public CommonReponseDto<MenuPagesDto> fetchMenuPages(String visibleRoles) {
		logger.info("MasterServiceImpl-fetchMenuPages-Start");
		CommonReponseDto<MenuPagesDto> response = new CommonReponseDto<MenuPagesDto>();
		try {
			MenuPages entity = menuPagesRepo.findByVisibleRolesAndIsActive(visibleRoles, CommonConstants.TRUE);
			if (null != entity && null != entity.getName()) {
				MenuPagesDto dto = new MenuPagesDto();
				BeanUtils.copyProperties(entity, dto);
				response.setResponsData(dto);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchMenuPages", e);
		}
		logger.info("MasterServiceImpl-fetchMenuPages-End");
		return response;
	}

	@Override
	public CommonReponseDto<FormPagesDto> fetchFormPage(String Moduletype) {
		logger.info("MasterServiceImpl-fetchFormPage-Start");
		CommonReponseDto<FormPagesDto> response = new CommonReponseDto<FormPagesDto>();
		try {
			FormPages entity = formPagesRepo.findByModuleTypeAndIsActive(Moduletype, CommonConstants.TRUE);
			if (null != entity && null != entity.getId()) {
				FormPagesDto dto = new FormPagesDto();
				BeanUtils.copyProperties(entity, dto);
				response.setResponsData(dto);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchFormPage", e);
		}
		logger.info("MasterServiceImpl-fetchFormPage-End");
		return response;
	}

	@Override
	public CommonDataTableDto fetchAllUsers(CommonDataTableDto dto) {
		logger.info("MasterServiceImpl-fetchAllUsers-Start");
		CommonDataTableDto response = new CommonDataTableDto();
		try {
			List<UserDetailsDto> dtos = new ArrayList<UserDetailsDto>();
			Page<UserDetailsEntity> pageData = null;
			if (null != dto.getFilter() && !dto.getFilter().equals("")) {
				JsonRequest<CommonSearchFieldDto> jsonRequest = new JsonRequest<>();
				CommonSearchFieldDto filter = jsonRequest.readEncoded(dto.getFilter(), CommonSearchFieldDto.class);
				pageData = filterDataWithSearch(dto, filter);
			} else {
				pageData = filterDataWithoutSearch(dto);
			}
			if (!pageData.getContent().isEmpty()) {
				for (UserDetailsEntity user : pageData.getContent()) {
					UserDetailsDto resdto = new UserDetailsDto();
					BeanUtils.copyProperties(user, resdto);
					resdto.setCreatedDate(DateUtils.convertDateTimeToStringWithPeriod(user.getCreatedDate()));
					resdto.setModifiedDate(DateUtils.convertDateTimeToStringWithPeriod(user.getModifiedDate()));
					resdto.setIsActive(Boolean.toString(user.isActive()));
					dtos.add(resdto);
				}
//				
				response.setData(dtos);
				response.setDraw(dto.getDraw());
				response.setRecordsTotal(pageData.getTotalElements());
				response.setCurrentPage(pageData.getPageable().getPageNumber() + 1);
				response.setLasPage(response.getCurrentPage() - 1);
				response.setNextPage(response.getCurrentPage() + 1);
				response.setRecordsFiltered((int) pageData.getTotalElements());
				response.setTotalPages(pageData.getTotalPages());
				response.setPerPageSize(pageData.getSize());
				response.setFrom(pageData.getPageable().getOffset() + 1);
				response.setTo(pageData.getPageable().getOffset() + Long.valueOf(pageData.getNumberOfElements()));
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_DATA_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchAllUsers", e);
		}
		logger.info("MasterServiceImpl-fetchAllUsers-End");
		return response;
	}

	private Page<UserDetailsEntity> filterDataWithoutSearch(CommonDataTableDto dto) {
		logger.info("MasterServiceImpl-filterDataWithoutSearch-Start");
		Page<UserDetailsEntity> usersData = null;
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<UserDetailsEntity> query = builder.createQuery(UserDetailsEntity.class);
			Root<UserDetailsEntity> root = query.from(UserDetailsEntity.class);
			List<Predicate> prediCateConditions = new ArrayList<>();
			Sort sort = Sort.by("modifiedDate").ascending();
			if (null != dto.getSort() && !dto.getSort().equalsIgnoreCase("")) {
				query = setSortOrder(query, builder, root, dto.getSort());
			}
			int pageNoo = dto.getCurrentPage() / dto.getPerPageSize();
			Pageable paging = PageRequest.of(pageNoo, dto.getPerPageSize(), sort);
			query.select(root).where(prediCateConditions.toArray(new Predicate[] {}));
			TypedQuery<UserDetailsEntity> entity = entityManager.createQuery(query);
			int totalRows = entity.getResultList().size();
			entity.setFirstResult(paging.getPageNumber() * paging.getPageSize());
			entity.setMaxResults(paging.getPageSize());
			usersData = new PageImpl<>(entity.getResultList(), paging, totalRows);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception in MasterServiceImpl-fetchAllUsers", e);
		}
		logger.info("MasterServiceImpl-filterDataWithoutSearch-End");
		return usersData;
	}

	private Page<UserDetailsEntity> filterDataWithSearch(CommonDataTableDto dto, CommonSearchFieldDto filter) {
		logger.info("MasterServiceImpl-filterDataWithSearch-Start");
		Page<UserDetailsEntity> usersData = null;
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<UserDetailsEntity> query = builder.createQuery(UserDetailsEntity.class);
			Root<UserDetailsEntity> root = query.from(UserDetailsEntity.class);
			List<Predicate> prediCateConditions = new ArrayList<>();

			prediCateConditions.add(builder.equal(root.get("isActive"), CommonConstants.TRUE));
			if (null != filter.getFirstName() && !filter.getFirstName().equalsIgnoreCase("")) {
				prediCateConditions.add(builder.equal(root.get("firstName"), filter.getFirstName()));
			}
			if (null != filter.getLastName() && !filter.getLastName().equalsIgnoreCase("")) {
				prediCateConditions.add(builder.equal(root.get("lastName"), filter.getLastName()));
			}

			Sort sort = Sort.by("modifiedDate").ascending();
			if (null != dto.getSort() && !dto.getSort().equalsIgnoreCase("")) {
				query = setSortOrder(query, builder, root, dto.getSort());
			}

			int pageNoo = dto.getCurrentPage() / dto.getPerPageSize();
			Pageable paging = PageRequest.of(pageNoo, dto.getPerPageSize(), sort);

			query.select(root).where(prediCateConditions.toArray(new Predicate[] {}));
			TypedQuery<UserDetailsEntity> entity = entityManager.createQuery(query);
			int totalRows = entity.getResultList().size();
			entity.setFirstResult(paging.getPageNumber() * paging.getPageSize());
			entity.setMaxResults(paging.getPageSize());
			usersData = new PageImpl<>(entity.getResultList(), paging, totalRows);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("MasterServiceImpl-filterDataWithSearch-End");
		return usersData;
	}

	private CriteriaQuery<UserDetailsEntity> setSortOrder(CriteriaQuery<UserDetailsEntity> query,
			CriteriaBuilder builder, Root<UserDetailsEntity> root, String sortBy) {
		String sortName = sortBy;
		String order = "";
		if (StringUtils.isNotBlank(sortBy)) {
			String[] splitted = sortBy.split("|");
			sortName = splitted[0];
			order = splitted.length >= 2 ? splitted[1] : "modifiedDate";
		}
		if (StringUtils.isNotBlank(order) && order.equalsIgnoreCase(CommonConstants.ASC)
				&& StringUtils.isNotBlank(sortName) && !sortName.equalsIgnoreCase(CommonConstants.UNDEFINED)) {
			if (sortName.equalsIgnoreCase("id")) {
				query.orderBy(builder.asc(root.get("id")));
			} else if (sortName.equalsIgnoreCase("firstName")) {
				query.orderBy(builder.asc(root.get("firstName")));
			} else if (sortName.equalsIgnoreCase("lastName")) {
				query.orderBy(builder.asc(root.get("lastName")));
			}

		} else if (StringUtils.isNotBlank(order) && order.equalsIgnoreCase(CommonConstants.DESC)
				&& StringUtils.isNotBlank(sortName) && !sortName.equalsIgnoreCase(CommonConstants.UNDEFINED)) {
			if (sortName.equalsIgnoreCase("id")) {
				query.orderBy(builder.desc(root.get("id")));
			} else if (sortName.equalsIgnoreCase("firstName")) {
				query.orderBy(builder.desc(root.get("firstName")));
			} else if (sortName.equalsIgnoreCase("lastName")) {
				query.orderBy(builder.desc(root.get("lastName")));
			}
		} else {
			query.orderBy(builder.desc(root.get("modifiedDate")));
		}
		return query;
	}

	@Override
	public CommonReponseDto<EmployeeDetailsDto> fetchEmployeeDetails() {
		logger.info("MasterServiceImpl-fetchEmployeeDetails-Start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		try {
			List<EmployeeDetails> entities = empDetailsRepository.findAll();
			if (null != entities && entities.size() > 0) {
				List<EmployeeDetailsDto> dtos = new ArrayList<EmployeeDetailsDto>();
				for (EmployeeDetails user : entities) {
					EmployeeDetailsDto resdto = new EmployeeDetailsDto();
					BeanUtils.copyProperties(user, resdto);
					dtos.add(resdto);
				}
				response.setResponsDatas(dtos);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchEmployeeDetails", e);
		}
		logger.info("MasterServiceImpl-fetchEmployeeDetails-End");
		return response;

	}

	@Override
	public CommonReponseDto<EmployeeDetailsDto> fetchAllEmployeedetails() {
		logger.info("MasterServiceImpl-fetchAllEmployeedetails-Start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		try {
			List<EmployeeDetails> entities = empDetailsRepository.findAll();
			if (null != entities && entities.size() > 0) {
				List<EmployeeDetailsDto> dtos = new ArrayList<EmployeeDetailsDto>();
				for (EmployeeDetails user : entities) {
					EmployeeDetailsDto resdto = new EmployeeDetailsDto();
					BeanUtils.copyProperties(user, resdto);
					dtos.add(resdto);
				}
				response.setResponsDatas(dtos);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchAllEmployeedetails", e);
		}
		logger.info("MasterServiceImpl-fetchAllEmployeedetails-End");
		return response;

	}

	@Override
	public CommonReponseDto<ProjectDetailsDto> fetchallProjects() {
		logger.info("MasterServiceImpl-fetchallProjects-Start");
		CommonReponseDto<ProjectDetailsDto> response = new CommonReponseDto<ProjectDetailsDto>();
		try {
			List<ProjectDetailsEntity> entities = projectDetailsRepository.findAll();
			if (null != entities && entities.size() > 0) {
				List<ProjectDetailsDto> dtos = new ArrayList<ProjectDetailsDto>();
				for (ProjectDetailsEntity user : entities) {
					ProjectDetailsDto resdto = new ProjectDetailsDto();
					BeanUtils.copyProperties(user, resdto);
					dtos.add(resdto);
				}
				response.setResponsDatas(dtos);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchallProjects", e);
		}
		logger.info("MasterServiceImpl-fetchallProjects-End");
		return response;
	}

	@Override
	public CommonReponseDto<TeamDetailsDto> fetchAllTeamDetails() {
		logger.info("MasterServiceImpl-fetchAllTeamDetails-Start");
		CommonReponseDto<TeamDetailsDto> response = new CommonReponseDto<TeamDetailsDto>();
		try {
			List<TeamDetails> entities = teamDetailsRepository.findAll();
			if (null != entities && entities.size() > 0) {
				List<TeamDetailsDto> dtos = new ArrayList<TeamDetailsDto>();
				for (TeamDetails user : entities) {
					TeamDetailsDto resdto = new TeamDetailsDto();
					BeanUtils.copyProperties(user, resdto);
					dtos.add(resdto);
				}
				response.setResponsDatas(dtos);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchAllTeamDetails", e);
		}
		logger.info("MasterServiceImpl-fetchAllTeamDetails-End");
		return response;
	}

	@Override
	public CommonReponseDto<ProjectDetailsDto> fetchProjectId(String payLoad) {
		logger.info("MasterServiceImpl-fetchProjectId-Start");
		CommonReponseDto<ProjectDetailsDto> response = new CommonReponseDto<ProjectDetailsDto>();
		JsonRequest<CommonRequestDto> jsonRequest = new JsonRequest<>();
		try {
			CommonRequestDto dto = jsonRequest.readEncoded(payLoad, CommonRequestDto.class);
			ProjectDetailsEntity entity = projectDetailsRepository.findByprojectIdAndIsActive(dto.getProjectId(),
					CommonConstants.TRUE);
			if (null != entity && null != entity.getProjectId()) {
				ProjectDetailsDto resdto = new ProjectDetailsDto();
				BeanUtils.copyProperties(entity, resdto);
				response.setResponsData(resdto);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchProjectId", e);
		}
		logger.info("MasterServiceImpl-fetchProjectId-End");
		return response;
	}

	@Override
	public CommonReponseDto<EmployeeDetailsDto> fetchEmployeeId(String payLoad) {
		logger.info("MasterServiceImpl-fetchEmployeeId-Start");
		CommonReponseDto<EmployeeDetailsDto> response = new CommonReponseDto<EmployeeDetailsDto>();
		JsonRequest<CommonRequestDto> jsonRequest = new JsonRequest<>();
		try {
			CommonRequestDto dto = jsonRequest.readEncoded(payLoad, CommonRequestDto.class);
			EmployeeDetails entity = empDetailsRepository.findByempidAndIsActive(dto.getEmpId(), CommonConstants.TRUE);
			if (null != entity && null != entity.getEmpid()) {
				EmployeeDetailsDto resdto = new EmployeeDetailsDto();
				BeanUtils.copyProperties(entity, resdto);
				response.setResponsData(resdto);
				response.setStatus(CommonConstants.SUCCESS);
			} else {
				response.setMessage(CommonConstants.NO_USER_FOUND);
				response.setStatus(CommonConstants.FAILURE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			logger.error("Exception in MasterServiceImpl-fetchEmployeeId", e);
		}
		logger.info("MasterServiceImpl-fetchEmployeeId-End");
		return response;
	}

}
