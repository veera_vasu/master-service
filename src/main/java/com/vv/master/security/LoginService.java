package com.vv.master.security;

import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.master.dto.UserDetailsDto;

/**
 * @author Veeravasu U
 *
 */
public interface LoginService {

	CommonReponseDto<UserDetailsDto> authenticate(LoginDto dto);

}
