package com.vv.master.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.master.controller.MasterController;
import com.vv.master.dto.UserDetailsDto;

/**
 * @author Veeravasu U
 * 
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/vv/login/api/")
public class LoginController {

	@Autowired
	LoginService loginService;

	Logger logger = LoggerFactory.getLogger(MasterController.class);

	@PostMapping(value = "authenticate")
	public ResponseEntity<?> saveUserDetails(@RequestBody LoginDto dto) {
		logger.info("LoginController-authenticate-start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<>();
		response = loginService.authenticate(dto);
		logger.info("LoginController-authenticate-End");
		return new ResponseEntity<CommonReponseDto<UserDetailsDto>>(response, HttpStatus.OK);
	}
}
