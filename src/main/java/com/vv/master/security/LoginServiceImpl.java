/**
 * 
 */
package com.vv.master.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vv.commonutilis.dto.CommonReponseDto;
import com.vv.commonutilis.services.CommonConstants;
import com.vv.master.controller.MasterController;
import com.vv.master.dto.UserDetailsDto;
import com.vv.master.entity.UserDetailsEntity;
import com.vv.master.repository.UserDetailsRepository;

import ch.qos.logback.core.joran.util.beans.BeanUtil;

/**
 * @author Veeravasu U
 *
 */
@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserDetailsRepository userDetailsRepository;

	Logger logger = LoggerFactory.getLogger(MasterController.class);

	@Override
	public CommonReponseDto<UserDetailsDto> authenticate(LoginDto dto) {
		logger.info("LoginServiceImpl-authenticate-start");
		CommonReponseDto<UserDetailsDto> response = new CommonReponseDto<>();
		try {
			if (null != dto.getUsername() && !dto.getUsername().equalsIgnoreCase("")) {
				UserDetailsEntity entity = userDetailsRepository.findByUseridAndIsActive(dto.getUsername(),
						CommonConstants.TRUE);
				if (null != entity) {
					UserDetailsDto user = new UserDetailsDto();
					BeanUtils.copyProperties(entity, user);
					response.setResponsData(user);
					response.setStatus(CommonConstants.SUCCESS);
				} else {
					response.setStatus(CommonConstants.ERROR);
					response.setStatus(CommonConstants.NO_USER_FOUND);
				}
			}else {
				response.setStatus(CommonConstants.ERROR);
				response.setStatus(CommonConstants.INVALID_USER);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(CommonConstants.ERROR);
			response.setStatus(CommonConstants.NO_USER_FOUND);
		}
		logger.info("LoginServiceImpl-authenticate-End");
		return response;
	}

}
